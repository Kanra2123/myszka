//Instrukcja podp�cia skrypt�w
//    Wersja: 0.3v
//    Data: 09.01.2022

//Skrypt "Movement"
//    Skrypt ten odpowiada za ruch postaci gracza (myszy) po mapie. Tak mysz jak i kamera powinny zosta�
//    wrzucone jako dzieci pustego obiektu. Movement powinien znale�� si� na tym pustym obiekcie. Konieczne
//    jest dodanie komponentu "Rigidbody" na pusty obiekt kt�ry jest rodzicem myszy i kamery. Ruch odbywa
//    si� za pomos� klawiszy wsad. Obr�t kamery mo�liwy jest po naci�ni�ciu Q lub E (Podobnie jak w don't starve)

//    Pola w skrypcie:
//        Move Speed: Odpowiedzialne jest za pr�dko�� ruchu gracza. Im wi�ksza warto��, tym szybszy ruch.        

//Skrypt UIMenager
//     Na scenie nale�y umie�ci� pusty obiekt, kt�ry b�dzie odpowiada� z aprzechowywanie na sobie 
//     menager�w. Na taki obiekt nale�y wstawi� skrypt

//	   Pola w skrypcie:
//	        temp: Wcze�niej utworzone pole tekstowe (zwyk�e, a nie TextMeshPro) w kt�rym maj� wy�wietla� si� punkty 

//Skrypt PointsCollecting
//     Skrypt nale�y umie�ci� na ka�dym obiekcie kt�rego zebranie ma zapewni� graczowi punkty.
//     Aby skrypt prawit�owo funkcjonowa�, mo�liwy do zebrania obiekt musi mie� komponent typu collider,
//     gdzie IsTrigger = true

//     Pola w skrypcie
//         UI Menager: W to pole trzeba podpi�� OBIEKT ze sceny kt�ry przechowuje menagery (while tym
//             skrypt "UIMenager")
//         Value: Pole to odpowiada za ilo�� dodanych punkt�w po zebraniu obiektu. Warto�� musi by� ca�kowita,
//             ale mo�e przyjmowa� warto�ci ujemne (Wtedy punkty zostan� odebrane)

//Skrypt EnemyDetectPlayer
//    Skrypt odpowiada za wykrywanie gracza przez przeciwnika. Gracz nie zostanie wykryty je�eli pomi�dzy nim
//    a przeciwnikiem znajdzie si� �ciana.
//    Aby skrypt dzia�a� prawid�owo, potrzebujemy obiektu przeciwnika (widoczny w grze model), i drugiego obiektu
//    (Najlepiej sto�ka) wskazuj�cego na pole widzenia przeciwnika. Sto�ek powinien zosta� ustawiony jako child modelu
//    przeciwnika. Collider sto�ka powinien zosta� ustawiony na "IsTrigger", a komponent MeshRenderer mo�e zosta� w 
//    razie potrzeby wy��czony (nie wp�ywa on na funkcjonowanie skryptu).
//    Skrypt nale�y przypi�� DO STO�KA reprezentuj�cego pole widzenia przeciwnika. Obecnie Skrypt po wykryciu gracza
//    wy�wietla stosowny komunikat w konsoli. W przysz�o�ci b�dzie w tym miejscu podpi�ty ekran informuj�cy o przegranej

//    Pola w skrypcie 
//        Enemy: W tym miejscu trzeba przypi�� model przeciwnika (parent sto�ka)
//    UWAGA!!! Sto�ek reprezentuje tak k�t jak i odleg�o�� widzenia. Gracz kt�ry nie wejdzie w kontakt ze sto�kiem nie
//    zostanie zauwa�ony, nawet je�li nie b�dzie ukryty za ainnym obiektem

//Skrypt EnemyRotation
//      Skrypt odpowiada za "rozgl�danie si�" przeciwnik�w. Nale�y wrzuci� go bezpo�rednio na obiekt b�d�cy modelem przeciwnika.
//      Pola Start Point i End Point okre�laj� w jakim zakresie stopni ma obraca� si� przeciwnik. 

//      Pola w skrypcie:
//          Start Point: Nale�y wpisa� warto�� od kt�rej ma zacz�� si� obraca� 
//          End Point: Warto�� na kt�rej ma przesta� si� obraca� i zacz�� obraca� si� w przeciwnym kierunku
//          Rotation Speed: Okre�la jak szybko obraca� b�dzie si� przeciwnik
//      UWAGA!!! Start Point musi by� MNIEJSZY od End Point, a pocz�tkowa rotacja przeciwnika musi zawiera� si� w przedziale
//      StartPoint< y < EndPoint
