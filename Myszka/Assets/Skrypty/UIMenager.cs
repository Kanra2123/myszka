using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIMenager : MonoBehaviour
{
    private int points = 0;
    [SerializeField]
    private Text temp;
    [SerializeField]
    private TextMeshProUGUI textMeshPro;

    private void Start()
    {
    }
    public void IncreasePointsByValue (int value)
    {
        points += value;
        temp.text = points.ToString();
        Debug.Log(points);
    }
}
