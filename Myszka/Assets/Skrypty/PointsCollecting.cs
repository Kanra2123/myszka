using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsCollecting : MonoBehaviour
{
    [SerializeField]
    private GameObject UIMenager;
    [SerializeField]
    private int value;

    public void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<Movement>())
        {
            UIMenager.GetComponent<UIMenager>().IncreasePointsByValue(value);
            this.gameObject.SetActive(false);
        }
    }
}
