using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDetectPlayer : MonoBehaviour
{
    [SerializeField]
    private GameObject enemy;
    private bool isDetected = false;

    public bool GetIsDetected()
    {
        return isDetected;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<Movement>())
        {
            Debug.Log("Collider");
            RaycastHit hit = new RaycastHit();
          //RaycastHit[] t =  Physics.RaycastAll(enemy.transform.position, Vector3.Normalize(other.transform.position - enemy.transform.position), Mathf.Infinity);
            Debug.DrawRay(enemy.transform.position, Vector3.Normalize(other.transform.position - enemy.transform.position));
            if (Physics.Raycast(enemy.transform.position, Vector3.Normalize(other.transform.position - enemy.transform.position), out hit, Mathf.Infinity))
            {
                Debug.Log(message: hit.collider.gameObject.name);
                Debug.Log(hit.collider.gameObject.GetComponent<Movement>());
                Debug.Log(hit.collider.gameObject.GetComponentInParent<Movement>());
                Debug.Log(hit.rigidbody.gameObject.name);
                Debug.Log(hit.rigidbody.gameObject.GetComponent<Movement>());
                isDetected = true;
                Debug.Log("Wykryto gracza");
            }
        }
        else
        {
            isDetected = false;
        }
    }
}
