using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRotation : MonoBehaviour
{
    [SerializeField] private int startPoint;
    [SerializeField] private int endPoint;
    [SerializeField] private int rotationSpeed;

    private bool fore = true;
    private bool moved = false;

    void Update()
    {     
        if((transform.eulerAngles.y < startPoint && moved)|| (transform.eulerAngles.y > endPoint && moved))
        {
            fore = !fore;
            moved = false;
        }
        if (fore)
        {
            transform.Rotate(Time.deltaTime * rotationSpeed * Vector3.down);
            moved = true;
        }
        else
        {
            transform.Rotate(Time.deltaTime * rotationSpeed * Vector3.up);
            moved = true;
        }
    }
}
