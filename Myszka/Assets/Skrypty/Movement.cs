using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField]
    private float moveSpeed = 5f;

    private Rigidbody rb;
    private Vector3 movement;
    private Vector3 rot;
    private bool isMoving = false;
    private bool isRotate = false;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }
    void Update()
    {
        rot = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z);

        TakeInfo();
    }

    private void TakeInfo()
    {
        float vertical = Input.GetAxis("Vertical");
        float horizontal = Input.GetAxis("Horizontal");

        isMoving = false;

        if (Input.GetAxis("Vertical") != 0)
        {
            movement =  moveSpeed * transform.forward * vertical;
            isMoving = true;
        }

        if (Input.GetAxis("Horizontal") != 0)
        {
            movement = moveSpeed * transform.right * horizontal;
            isMoving = true;
        }

        if (isMoving)
            Move(movement);

        isRotate = false;

        if (Input.GetKeyDown(KeyCode.Q))
        {
            rot.y -= 90;
            isRotate = true;
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            rot.y += 90;
            isRotate = true;
        }

        if (isRotate)
        {
            Rotate(rot);
        }
    }
    private void Rotate(Vector3 vector)
    {
        transform.rotation = Quaternion.Euler(rot);
    }
    private void Move(Vector3 vector)
    {
        rb.velocity = vector;
    }
}
